

import java.util.Random;

public class Book{

    static int nLibri=0;
    static Random rnd=new Random();
    String title;
    String author;
    int pages;

    public void Book(String title, String author, int pages){                        //costruttore con argomenti
            nLibri++;
            this.title=title;
            this.author=author;
            this.pages=pages;
    }

    public void Book(){                                                              //costruttore di default(nel caso non volessimo mettere argomenti
            nLibri++;
    }

    public static void bookSwap(Book b1, Book b2){
            String keepTitle="";
            String keepAuthor="";
            int keepPages=0;

            keepTitle=b1.title;
            keepAuthor=b1.author;
            keepPages=b1.pages;

            b1.title=b2.title;
            b1.author=b2.author;
            b1.pages=b2.pages;

            b2.title=keepTitle;
            b2.author=keepAuthor;
            b2.pages=keepPages;
    }

    public static void bookSwapArr(bookList[] b,int i, int k){
            Book keep= new Book();
            keep=b[i];
            b[i]=b[j];
            b[j]=keep;
    }

    public static void titleSort(Book[] b){
        for(int i=0;i<b.length;i++){
                for (int k=i+1;k<b.length;k++) {
                        if(b[i].title.compareTo(b[k].title)<0){
                            bookSwap(b[i], b[k]);
                        }
                }
        }
        return;
    }

    public static void authorSort(Book[] b){
        for(int i=0;i<b.length;i++)
                for (int k = i+1; k < b.length;  k++) {
                        if(b[i].author.compareTo(b[k].author)<0){
                            bookSwap(b[i], b[k]);
                        }
                }
        return;
    }
    
    public static Book getRandomBook(){
            Book rndB=new Book();

            String rndTitle="";
            String rndAuthor="";
            int rndPages=0;
            int titlelength=10;
            int authorLength=8;
            char a='a';

            for(int i=0;i<titlelength;i++)
                    rndTitle+=(char)(a+rnd.nextInt(25));
            for(int i=0;i<authorLength;i++)
                    rndAuthor+=(char)(a+rnd.nextInt(25));        
            rndPages=(rnd.nextInt(20)+80);

            rndB.title=rndTitle;
            rndB.author=rndAuthor;
            rndB.pages=rndPages;
            nLibri++;

            return rndB;
    }

    public String getTitle(){
        return title;
    }

    public String getAuthor(){
        return author;
    }

    public int getPages(){
        return pages;
    }

    public String toString(){
        return title+" "+author+" "+pages;
    }

    public static void main(String[] args){
        Book[] bookList=new Book[10];

        for(int i=0;i<bookList.length;i++)
                bookList[i]= getRandomBook();
    
        System.out.println("You have: "+bookList.length+ " books");
        for(int i=0;i<bookList.length;i++)
                System.out.println(bookList[i].title+" "+bookList[i].author);

        titleSort(bookList);

        System.out.println();
        for(int i=0;i<bookList.length;i++)
                System.out.println(bookList[i].title+" "+bookList[i].author);
    }

}